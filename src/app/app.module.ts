import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovieComponent } from './movie/movie.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HistorysComponent } from './historys/historys.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieComponent,
    LoginComponent,
    HomeComponent,
    HistorysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
